var path = require('path');
var argv = require('yargs').argv;
var _path = argv.root ? process.cwd() : '';
var bodyParser = require('body-parser');
var apiServer = require('api-server');
var server = apiServer.create();
var logger = require('morgan');
var routes = require(path.join(_path, './routes.json'));
var error = require(path.join(_path, './error.json'));
var request = require('request');
var db = require(path.join(_path, './db.json'));
var router = apiServer.router('./db.json');
var middlewares = apiServer.defaults();
var schema = require(path.join(_path, './schema.json'));
var Validator = require('jsonschema').Validator;
var errorArg = false;

// Check error flags
if (parseInt(argv.error) === 1) {
  errorArg = 1;
} else if (parseInt(argv.error) === 2) {
  errorArg = 2;
}

var clientErrorHandler = function(err, req, res, next) {
  if (req.method === 'POST' && req.body) {
    res.status(400).jsonp(error.malformedJSON);
  } else {
    next(err);
  }
};

// Add this before server.use(router)
server.use(apiServer.rewriter(routes));
server.use(logger('dev'));
// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares, bodyParser.json(), bodyParser.urlencoded({
  extended: true
}));
server.use(clientErrorHandler);

// In this example, returned resources will be wrapped in a body property
router.render = function(req, res) {
  if (res.locals.data) {
    res.locals.data.mock = true;
  }
  res.jsonp(res.locals.data);
};

routes.middlewares.forEach((item) => {
  server.post(item.src, (req, res, next) => {
    if (schema[req.url]) {
      var v = new Validator();
      console.log(req.body);
      var checkValidation = v.validate(req.body, schema[req.url]);
      if (checkValidation.errors.length) {
        console.info(checkValidation)
        error.schema.message = checkValidation.errors;
        res.status(500).jsonp(error.schema);
        next();
      }
    }
    if (item.headers) {
      console.log('Headers', item.headers);
      res.set(item.headers);
    }
    if (!errorArg) {
      if (item.redirect) {
        res.redirect(item.redirect);
      } else if (item.entry) {
        db[item.entry].mock = true;
        res.status(db[item.entry].status || db[item.entry]._embedded.status || 200).jsonp(db[item.entry]);
      }
    } else if (errorArg === 1) {
      // client errors
    } else if (errorArg === 2) {
      // server errors
      db.serverError.mock = true;
      res.status(500).jsonp(db.serverError);
    }
  });
});

// Use default router
server.use(router);
server.listen(3000, function() {
  console.log('API Server is running');
});
